package com.xsisacademy.bootcamp.repository;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import com.xsisacademy.bootcamp.model.Product;

public interface ProductRepository extends JpaRepository<Product, Long>{
	
	//delete Product by Variant
	
	@Modifying
	@Query(value = "UPDATE Product p"
			+ " SET is_active = false"
			+ " WHERE variant_id = ?1", nativeQuery = true)
	@Transactional
	public void deleteProductByVariantId(Long id);
	
	//delete Product by Category
	
	@Modifying
	@Query(value = "UPDATE Product p"
			+ " SET is_active = false"
			+ " WHERE category_id = ?1", nativeQuery = true)
	@Transactional
	public void deleteProductByCategoryId(Long id);
	
	//delete Product by id
	
	@Modifying
	@Query(value = "UPDATE Product p"
			+ " SET is_active = false"
			+ " WHERE p.id = ?1", nativeQuery = true)
	@Transactional
	public void deleteProductById(Long id);
	
	//ngerubah status=false, jadi status=true
	
	@Modifying
	@Query(value = "UPDATE Product p"
			+ " SET is_active = true"
			+ " WHERE category_id = ?1", nativeQuery = true)
	@Transactional
	public void showProductByCategoryId(Long id);
	
}
