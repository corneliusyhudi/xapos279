package com.xsisacademy.bootcamp.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.xsisacademy.bootcamp.model.OrderHeader;

public interface OrderHeaderRepository extends JpaRepository<OrderHeader,Long>{

	@Query("SELECT MAX(id) AS Max_id FROM OrderHeader")
	public Long getMaxOrderHeader();
		
}
