package com.xsisacademy.bootcamp.repository;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import com.xsisacademy.bootcamp.model.Category;
import com.xsisacademy.bootcamp.model.Variant;

public interface VariantRepository extends JpaRepository<Variant, Long>{
	
	//delete
	
	@Modifying
	@Query(value = "UPDATE Variant v"
			+ " SET is_active = false"
			+ " WHERE category_id = ?1", nativeQuery = true)
	@Transactional
	public void deleteVariantByCategoryId(Long id);
	
	//ngerubah status=false, jadi status=true
	
	@Modifying
	@Query(value = "UPDATE Variant v"
			+ " SET is_active = true"
			+ " WHERE category_id = ?1", nativeQuery = true)
	@Transactional
	public void showVariantByCategoryId(Long id);
	
	@Modifying
	@Query(value = "UPDATE Variant v SET is_active = false WHERE v.id = ?1", nativeQuery = true)
	@Transactional
	public void deleteVariantById(Long id);
}
