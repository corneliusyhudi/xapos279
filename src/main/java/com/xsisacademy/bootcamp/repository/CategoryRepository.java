package com.xsisacademy.bootcamp.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import com.xsisacademy.bootcamp.model.Category;
import java.util.*;

import javax.transaction.Transactional;

public interface CategoryRepository extends JpaRepository<Category, Long> {
	
	public Page<Category>findByIsActiveTrueOrderByCategoryName(Pageable pageable);
	
	public Category findByCategoryName(String categoryName);
	public Category findByCategoryCode(String categoryCode);
	
	@Modifying
	@Query(value = "UPDATE Category c SET is_active = false WHERE c.id = ?1", nativeQuery = true)
	@Transactional
	public void deleteCategoryById(Long id);
	
	@Query("FROM Category WHERE LOWER(categoryName) LIKE LOWER(CONCAT('%' , ?1 , '%'))")
	public List<Category> searchCategory(String keyword);
	
}
