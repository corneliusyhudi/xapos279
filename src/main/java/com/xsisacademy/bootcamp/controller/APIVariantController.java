package com.xsisacademy.bootcamp.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.*;

import com.xsisacademy.bootcamp.model.Category;
import com.xsisacademy.bootcamp.model.Variant;
import com.xsisacademy.bootcamp.repository.ProductRepository;
import com.xsisacademy.bootcamp.repository.VariantRepository;

@RestController
@CrossOrigin("*")
@RequestMapping("/api/")
public class APIVariantController {

	@Autowired
	public VariantRepository variantRepository;
	
	@Autowired 
	public ProductRepository productRepository;
	
	@GetMapping("variant")
	public ResponseEntity<List<Variant>> getAllVariant(){
		
		try {
			
			List<Variant> variant = this.variantRepository.findAll();
			return new ResponseEntity<>(variant, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		}
		
	}
	
	@PostMapping("add/variant")
	public ResponseEntity<Object> saveVariant(@RequestBody Variant variant){
		variant.setIsActive(true);
		Variant variantData = this.variantRepository.save(variant);
		if(variantData.equals(variant)) {
			return new ResponseEntity<Object>("Save Data Succesfully", HttpStatus.OK);
		} else {
			return new ResponseEntity<Object>("Save Data Failed", HttpStatus.BAD_REQUEST);
		}
	}
	
	@GetMapping("variant/{id}")
	public ResponseEntity<List<Variant>> getVariantById(@PathVariable("id") Long id){
		try {
			Optional<Variant> variant = this.variantRepository.findById(id);
			if (variant.isPresent()) {
				ResponseEntity rest = new ResponseEntity<>(variant, HttpStatus.OK);
				return rest;
			} else {
				return ResponseEntity.notFound().build();
			}
		} catch (Exception e) {
			return new ResponseEntity<List<Variant>>(HttpStatus.NO_CONTENT);
		}
	}
	
	@PutMapping("edit/variant/{id}")
	public ResponseEntity<Object> updatedCategory(@RequestBody Variant variant, @PathVariable("id")Long id){
		variant.setIsActive(true);
		Optional<Variant> categoryData = this.variantRepository.findById(id);
		if (categoryData.isPresent()) {
			variant.setId(id);
			this.variantRepository.save(variant);
			ResponseEntity rest = new ResponseEntity<>("Updated Success", HttpStatus.OK);
			return rest;
		} else {
			return ResponseEntity.notFound().build();
		}
	}
	
	@PutMapping("delete/variant/{id}")
	public ResponseEntity<Object> deleteVariant(@PathVariable("id") Long id) {
		Optional<Variant> variantData = this.variantRepository.findById(id);
		if(variantData.isPresent()) {
			Variant variant = new Variant();
			variant.setId(id);
			this.variantRepository.deleteVariantById(id);
			this.productRepository.deleteProductByVariantId(id);
			ResponseEntity rest = new ResponseEntity<>("Deleted Success", HttpStatus.OK);
			return rest;
		} else {
			return ResponseEntity.notFound().build();
		}
	}
	
}
