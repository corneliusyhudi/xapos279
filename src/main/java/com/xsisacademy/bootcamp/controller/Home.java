package com.xsisacademy.bootcamp.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping("/")
public class Home {
	
	@GetMapping("")
	public ModelAndView index() {
		ModelAndView view = new ModelAndView("home/index");
		return view;
	}
	
	@GetMapping("form")
	public ModelAndView form() {
		ModelAndView view = new ModelAndView("home/form");
		return view;
	}
	
	@GetMapping("calculator1")
	public ModelAndView calculator1() {
		ModelAndView view = new ModelAndView("home/calculator1");
		return view;
	}
	
	@GetMapping("calculator2")
	public ModelAndView calculator2() {
		ModelAndView view = new ModelAndView("home/calculator2");
		return view;
	}
	
	
	
}
