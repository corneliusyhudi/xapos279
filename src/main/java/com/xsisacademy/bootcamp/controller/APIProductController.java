package com.xsisacademy.bootcamp.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.xsisacademy.bootcamp.model.Product;
import com.xsisacademy.bootcamp.model.Variant;
import com.xsisacademy.bootcamp.repository.ProductRepository;

import java.util.*;

@RestController
@CrossOrigin("*")
@RequestMapping("/api/")
public class APIProductController {
	
	@Autowired
	public ProductRepository productRepository;
	
	@GetMapping("product")
	public ResponseEntity<List<Product>> getAllProduct(){
		try {
			
			List<Product> product = this.productRepository.findAll();
			return new ResponseEntity<>(product, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		}
	}
	
	@PostMapping("add/product")
	public ResponseEntity<Object> saveProduct(@RequestBody Product product){
		product.setIsActive(true);
		Product productData = this.productRepository.save(product);
		if(productData.equals(product)) {
			return new ResponseEntity<Object>("Save Data Succesfully", HttpStatus.OK);
		} else {
			return new ResponseEntity<Object>("Save Data Failed", HttpStatus.BAD_REQUEST);
		}
	}
	
	@GetMapping("product/{id}")
	public ResponseEntity<List<Product>> getVariantById(@PathVariable("id") Long id){
		try {
			Optional<Product> product = this.productRepository.findById(id);
			if (product.isPresent()) {
				ResponseEntity rest = new ResponseEntity<>(product, HttpStatus.OK);
				return rest;
			} else {
				return ResponseEntity.notFound().build();
			}
		} catch (Exception e) {
			return new ResponseEntity<List<Product>>(HttpStatus.NO_CONTENT);
		}
	}
	
	@PutMapping("edit/product/{id}")
	public ResponseEntity<Object> updatedProduct(@RequestBody Product product, @PathVariable("id")Long id){
		product.setIsActive(true);
		Optional<Product> productData = this.productRepository.findById(id);
		if (productData.isPresent()) {
			product.setId(id);
			this.productRepository.save(product);
			ResponseEntity rest = new ResponseEntity<>("Updated Success", HttpStatus.OK);
			return rest;
		} else {
			return ResponseEntity.notFound().build();
		}
	}
	
	@PutMapping("delete/product/{id}")
	public ResponseEntity<Object> deleteVariant(@PathVariable("id") Long id) {
		Optional<Product> productData = this.productRepository.findById(id);
		if(productData.isPresent()) {
			Product product = new Product();
			product.setId(id);
			this.productRepository.deleteProductById(id);
			ResponseEntity rest = new ResponseEntity<>("Deleted Success", HttpStatus.OK);
			return rest;
		} else {
			return ResponseEntity.notFound().build();
		}
	}
	
}
