package com.xsisacademy.bootcamp.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.query.Param;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.*;

import com.xsisacademy.bootcamp.model.Category;
import com.xsisacademy.bootcamp.repository.CategoryRepository;
import com.xsisacademy.bootcamp.repository.ProductRepository;
import com.xsisacademy.bootcamp.repository.VariantRepository;

@RestController
@CrossOrigin("*")
@RequestMapping("/api/")
public class APICategoryController {
	
	@Autowired
	public CategoryRepository categoryRepository;
	
	@Autowired
	public VariantRepository variantRepository;
	
	@Autowired 
	public ProductRepository productRepository;
	
	@GetMapping("category")
	public ResponseEntity<List<Category>> getAllCategory(){
		
		try {
			List<Category> category = this.categoryRepository.findAll();
			return new ResponseEntity<>(category, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		}
		
	}
	
	@PostMapping("add/category")
	public ResponseEntity<Object> saveCategory(@RequestBody Category category){

//		Category databaseCategory = this.categoryRepository.findByCategoryName(category.getCategoryName()); //dapetin categoryNama dari repo
//		System.out.println(databaseCategory);
//		return new ResponseEntity<Object>("Save Data Succesfully", HttpStatus.OK);
		category.setIsActive(true);
		Category categoryData = this.categoryRepository.save(category);
		if(categoryData.equals(category)) {
			return new ResponseEntity<Object>("Save Data Succesfully", HttpStatus.OK);
		} else {
			return new ResponseEntity<Object>("Save Data Failed", HttpStatus.BAD_REQUEST);
		}
	}
	
	@GetMapping("category/{id}")
	public ResponseEntity<List<Category>> getCategoryById(@PathVariable("id") Long id){
		try {
			Optional<Category> category = this.categoryRepository.findById(id);
			if (category.isPresent()) {
				this.variantRepository.showVariantByCategoryId(id);
				ResponseEntity rest = new ResponseEntity<>(category, HttpStatus.OK);
				return rest;
			} else {
				return ResponseEntity.notFound().build();
			}
		} catch (Exception e) {
			return new ResponseEntity<List<Category>>(HttpStatus.NO_CONTENT);
		}
	}
	
	@PutMapping("edit/category/{id}")
	public ResponseEntity<Object> updatedCategory(@RequestBody Category category, @PathVariable("id")Long id){
		category.setIsActive(true);
		Optional<Category> categoryData = this.categoryRepository.findById(id);
		if (categoryData.isPresent()) {
			category.setId(id);
			this.categoryRepository.save(category);
			ResponseEntity rest = new ResponseEntity<>("Updated Success", HttpStatus.OK);
			return rest;
		} else {
			return ResponseEntity.notFound().build();
		}
	}
	
	@PutMapping("delete/category/{id}")
	public ResponseEntity<Object> deleteCategory(@PathVariable("id") Long id) {
		Optional<Category> categoryData = this.categoryRepository.findById(id);
		if(categoryData.isPresent()) {
			Category category = new Category();
			category.setId(id);
			this.categoryRepository.deleteCategoryById(id);
			this.variantRepository.deleteVariantByCategoryId(id);
			this.productRepository.deleteProductByCategoryId(id);
			ResponseEntity rest = new ResponseEntity<>("Deleted Success", HttpStatus.OK);
			return rest;
		} else {
			return ResponseEntity.notFound().build();
		}
	}
	
	@PostMapping("category/search")
	public ResponseEntity<List<Category>> getCategoryByName(@Param("keyword") String keyword) {
		if (keyword.equals("")) {
			List<Category> category = this.categoryRepository.findAll();
			return new ResponseEntity<List<Category>>(category, HttpStatus.OK);
		} else {
			List<Category> category = this.categoryRepository.searchCategory(keyword);
			return new ResponseEntity<List<Category>>(category, HttpStatus.OK);
		}
	}
	
	@GetMapping("category/paging")
	public ResponseEntity<Map<String, Object>> getAllCategories(@RequestParam(defaultValue="0") int page, @RequestParam(defaultValue="5") int size) {
		try {
			List<Category> category = new ArrayList<>();
			Pageable pagingSort = PageRequest.of(page, size);
			
			Page<Category> pageTuts;
			pageTuts = categoryRepository.findByIsActiveTrueOrderByCategoryName(pagingSort);
			category = pageTuts.getContent();
			
			Map<String, Object> response = new HashMap<>();
			response.put("category", category);
			response.put("currentPage", pageTuts.getNumber());
			response.put("totalItems", pageTuts.getTotalElements());
			response.put("totalPages", pageTuts.getTotalPages());
			
			return new ResponseEntity<>(response, HttpStatus.OK);
		} catch(Exception e) {
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
}








































