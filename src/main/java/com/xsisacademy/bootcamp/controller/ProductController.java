package com.xsisacademy.bootcamp.controller;

import java.util.*;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.xsisacademy.bootcamp.model.Category;
import com.xsisacademy.bootcamp.model.Product;
import com.xsisacademy.bootcamp.model.Variant;
import com.xsisacademy.bootcamp.repository.CategoryRepository;
import com.xsisacademy.bootcamp.repository.ProductRepository;
import com.xsisacademy.bootcamp.repository.VariantRepository;

@Controller
@RequestMapping("/product/")
public class ProductController {

	@Autowired
	private ProductRepository productRepository;
	
	@Autowired
	private VariantRepository variantRepository;
	
	@Autowired
	private CategoryRepository categoryRepository;
	
	@GetMapping("index")
	public ModelAndView index() {
		ModelAndView view = new ModelAndView("product/index");
		List<Product> listProduct = this.productRepository.findAll();
		view.addObject("listProduct",listProduct);
		return view;
	}
	
	@GetMapping("indexapi")
	public ModelAndView indexapi() {
		ModelAndView view = new ModelAndView("product/indexapi");
		return view;
	}
	
	@GetMapping("addformproduct")
	public ModelAndView addForm() {
		ModelAndView view = new ModelAndView("product/addformproduct");
		Product product = new Product();
		view.addObject("product",product);
		
		List<Category> listCategory = this.categoryRepository.findAll();
		view.addObject("listCategory",listCategory);
		
		List<Variant> listVariant = this.variantRepository.findAll();
		view.addObject("listvariant",listVariant);
		return view;
	}
	
	@PostMapping("save")
	public ModelAndView save(@ModelAttribute Product product, BindingResult result) {
		if(!result.hasErrors()) {
			this.productRepository.save(product);
			return new ModelAndView("redirect:/product/index");
		} else {
			return new ModelAndView("redirect:/product/index");
		}
	}
	
	@GetMapping("edit/{id}")
	public ModelAndView edit(@PathVariable("id") Long id) {
		ModelAndView view = new ModelAndView("product/addformproduct");
		Product product = this.productRepository.findById(id).orElse(null);
		view.addObject("product", product);
		
		List<Variant> listVariant = this.variantRepository.findAll();
		view.addObject("listvariant",listVariant);
		
		List<Category> listCategory = this.categoryRepository.findAll();
		view.addObject("listcategory",listCategory);
		return view;
	}
	
	@GetMapping("delete/{id}")
	public ModelAndView delete(@PathVariable("id") Long id) {
		if(id != null) {
			this.productRepository.deleteById(id);
		}
		return new ModelAndView("redirect:/product/index");
	}
	
}
