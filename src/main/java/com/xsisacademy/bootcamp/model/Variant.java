package com.xsisacademy.bootcamp.model;

import javax.persistence.*;


@Entity
@Table(name = "variant")
public class Variant {
	
	//Table: Variant
	//Field / Column : id,variant_code,variant_name
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@Column(name = "variant_code")
	private String variantCode;
	
	@Column(name = "variant_name")
	private String variantName;
	
	@Column(name = "category_id")
	private Long categoryId;
	
	@Column(name = "is_active")
	private Boolean isActive;
	

	@ManyToOne
	@JoinColumn(name = "category_id", insertable = false, updatable = false)
	public Category category;

	public Boolean getIsActive() {
		return isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}
	
	public Category getCategory() {
		return category;
	}

	public void setCategory(Category category) {
		this.category = category;
	}

	public Long getCategoryId() {
		return categoryId;
	}

	public void setCategoryId(Long categoryId) {
		this.categoryId = categoryId;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getVariantCode() {
		return variantCode;
	}

	public void setVariantCode(String variantCode) {
		this.variantCode = variantCode;
	}

	public String getVariantName() {
		return variantName;
	}

	public void setVariantName(String variantName) {
		this.variantName = variantName;
	}
	
	


	
}
