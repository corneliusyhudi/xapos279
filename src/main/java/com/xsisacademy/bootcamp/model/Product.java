package com.xsisacademy.bootcamp.model;

import javax.persistence.*;


@Entity
@Table(name = "product")
public class Product {
	
//	Buat Modul Table Product
//	Field / Column : id, variant_id, product_code, product_name, 
//	product_description, product_price, product_stock

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@Column(name = "variant_id")
	private Long variantId;
	
	@Column(name = "category_id")
	private Long categoryId;
	
	@Column(name = "product_code")
	private String productCode;
	
	@Column(name = "product_name")
	private String productName;
	
	@Column(name = "product_description")
	private String productDescription;
	
	@Column(name = "product_price")
	private Long productPrice;
	
	@Column(name = "product_stock")
	private Long productStock;
	
	@Column(name = "is_active")
	private Boolean isActive;
	

	@ManyToOne
	@JoinColumn(name = "variant_id", insertable = false, updatable = false)
	public Variant variant;
	
	@ManyToOne
	@JoinColumn(name = "category_id", insertable = false, updatable = false)
	public Category category;
	
	
	
	public Long getCategoryId() {
		return categoryId;
	}

	public void setCategoryId(Long categoryId) {
		this.categoryId = categoryId;
	}

	public Boolean getIsActive() {
		return isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}
	
	public Category getCategory() {
		return category;
	}

	public void setCategory(Category category) {
		this.category = category;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getVariantId() {
		return variantId;
	}

	public void setVariantId(Long variantId) {
		this.variantId = variantId;
	}

	public String getProductCode() {
		return productCode;
	}

	public void setProductCode(String productCode) {
		this.productCode = productCode;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public String getProductDescription() {
		return productDescription;
	}

	public void setProductDescription(String productDescription) {
		this.productDescription = productDescription;
	}

	public Long getProductPrice() {
		return productPrice;
	}

	public void setProductPrice(Long productPrice) {
		this.productPrice = productPrice;
	}

	public Long getProductStock() {
		return productStock;
	}

	public void setProductStock(Long productStock) {
		this.productStock = productStock;
	}

	public Variant getVariant() {
		return variant;
	}

	public void setVariant(Variant variant) {
		this.variant = variant;
	}

	
	
	
	
	
}
